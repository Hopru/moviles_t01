package com.iteso.tarea01;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.PersistableBundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.MessageFormat;

public class ActivityMain extends AppCompatActivity {


    public class Student {
        private String name = "";
        private String phone = "";
        private String scholarity = "";
        private String genre = "";
        private String favoriteBook = "";
        private String practiceSport = "";

        public void setName(String name) {
            this.name = name;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public void setScholarity(String scholarity) {
            this.scholarity = scholarity;
        }

        public void setGenre(String genre) {
            this.genre = genre;
        }

        public void setFavoriteBook(String favoriteBook) {
            this.favoriteBook = favoriteBook;
        }

        public void setPracticeSport(String practiceSport) {
            this.practiceSport = practiceSport;
        }

        private void setProperties(String name, String phone, String scholarity, String genre, String favoriteBook, String practiceSport) {
            this.name = name;
            this.phone = phone;
            this.scholarity = scholarity;
            this.genre = genre;
            this.favoriteBook = favoriteBook;
            this.practiceSport = practiceSport;
        }

        @Override
        public String toString() {
            String favoriteBookString;
            if(this.favoriteBook == null || this.favoriteBook == "") {
                favoriteBookString = "";
            } else {
                favoriteBookString = "Libro favorito: " + this.favoriteBook + "\n";
            }
            String message = MessageFormat.format("Nombre: {0} \n" +
                    "Numero: {1} \n" +
                    "Escolaridad: {2} \n" +
                    "Genero: {3}\n" +
                    "{4}" +
                    "Practica Deporte: {5}",this.name, this.phone, this.scholarity, this.genre, favoriteBookString, this.practiceSport);
            return message;
        }
    }

    Student student;
    EditText name;
    EditText phone;
    RadioGroup genre;
    Spinner scholarity;
    AutoCompleteTextView favoriteBook;
    Button clear;
    CheckBox practiceSports;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        scholarity = (Spinner) findViewById(R.id.activity_main_spinner_escolaridad);
        favoriteBook = (AutoCompleteTextView) findViewById(R.id.activity_main_actv_favorite_book);
        name = findViewById(R.id.activity_main_et_name);
        phone = findViewById(R.id.activity_main_et_phone);
        genre = findViewById(R.id.activity_main_rg_genre);
        practiceSports = findViewById(R.id.activity_main_cb_practice_sport);


        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.activity_main_spinner_escolaridad, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        scholarity.setAdapter(adapter);


        ArrayAdapter<CharSequence> autoCompleteAdapter = ArrayAdapter.createFromResource(this, R.array.activity_main_actv_favorite_book, android.R.layout.simple_dropdown_item_1line);
        autoCompleteAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        favoriteBook.setAdapter(autoCompleteAdapter);


        if(savedInstanceState != null) {
            restore(savedInstanceState);
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        restore(savedInstanceState);
    }

    public void restore(Bundle savedInstanceState) {
        name.setText(savedInstanceState.getString("NAME"));
        phone.setText(savedInstanceState.getString("PHONE"));
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        outState.putString("NAME", name.getText().toString());
        outState.putString("PHONE", name.getText().toString());
        super.onSaveInstanceState(outState, outPersistentState);
    }

    public void onClear(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityMain.this);
        builder.setMessage("Desea limpiar el contenido?")
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        clearFields();
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        builder.show();
    }

    public void clearFields() {
        student = new Student();
        name.setText("");
        phone.setText("");
        favoriteBook.setText("");
        genre.check(-1);
        scholarity.setSelection(0);
        practiceSports.setChecked(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.activity_main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.activity_main_save:
                student = new Student();
                student.setName(name.getText().toString());
                student.setPhone(phone.getText().toString());
                student.setFavoriteBook(favoriteBook.getText().toString());
                student.setScholarity(scholarity.getSelectedItem().toString());

                if(genre.getCheckedRadioButtonId()!=-1){
                    int id= genre.getCheckedRadioButtonId();
                    View radioButton = genre.findViewById(id);
                    int radioId = genre.indexOfChild(radioButton);
                    RadioButton btn = (RadioButton) genre.getChildAt(radioId);
                    student.setGenre(btn.getText().toString());
                }

                if(practiceSports.isChecked()) {
                    student.setPracticeSport("Si");
                } else {
                    student.setPracticeSport("No");
                }

                LayoutInflater inflater = this.getLayoutInflater();
                View layout = inflater.inflate(R.layout.activity_main_save_toast, (ViewGroup) ActivityMain.this.findViewById(R.id.activity_main_save_toast_text));

                TextView textView = layout.findViewById(R.id.activity_main_save_toast_text);

                textView.setText(student.toString());

                Toast custom = new Toast(ActivityMain.this);
                custom.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
                custom.setDuration(Toast.LENGTH_SHORT);
                custom.setView(layout);
                custom.show();
                clearFields();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
